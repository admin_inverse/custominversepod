
Pod::Spec.new do |s|


  s.name         = "InversePod"
  s.version      = "0.0.1"
  s.summary      = "Inverse Pod is used for testing the internal pod project."
  s.homepage     = "http://inverse.rs"

  s.license      = {:type => "MIT", :file => "LICENSE"}

  s.author    = "Ognjen Manevski"

  s.platform     = :ios
  s.ios.deployment_target = "8.0"
  s.source       = { :git => "https://admin_inverse@bitbucket.org/admin_inverse/custominversepod.git", :tag => "#{s.version}" }

  s.dependency 'Alamofire', '~> 4.3.0'
  s.dependency 'MBProgressHUD', '~> 1.0.0'

  s.source_files = "InversePod/**/*.{h,swift}"
  s.resource_bundle = {'InversePod' => ['InversePod/**/*.{xib,png}','InversePod/Media.xcassets']}

end
