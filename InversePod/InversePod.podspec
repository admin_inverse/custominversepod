
Pod::Spec.new do |s|


  s.name         = "InversePod"
  s.version      = "0.0.1"
  s.summary      = "Inverse Pod is used for testing the internal pod project."
  s.homepage     = "http://inverse.rs"

  s.framework = "UIKit"
  s.dependency 'Alamofire', '~> 2.0'
  s.dependency 'MBProgressHUD', '~> 0.9.0'

  s.license      = "License"

  s.author    = "Ognjen Manevski"

  s.platform     = :ios
  s.ios.deployment_target = "8.0"
  s.source       = { :git => "https://admin_inverse@bitbucket.org/admin_inverse/custominversepod.git", :tag => "#{s.version}" }

  s.source_files  = "InversePod/**/*.{swift}"
  s.resources = "InversePod/**/*.{png,jpeg,jpg,storyboard,xib}"


end
